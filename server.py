"""Simple HTTP Server to serve Transport App index.html and static files.
"""

import os

from flask import Flask

app = Flask(
    __name__,
    static_url_path='',
    static_folder=os.path.dirname(os.path.abspath(__file__)))

app.config.from_pyfile('flaskapp.cfg')


@app.route('/')
def index():
    """
    """
    return app.send_static_file('index.html')


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000)

