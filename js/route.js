// A Backbone application for learning purposes.
// Uses localstorage to persist Backbone models within browser.

$(function(){

    // Route Model
    // ----------

    var Route = Backbone.Model.extend({

        selectOptions: {
            "bus": "Bus",
            "train": "Train",
            "plane": "Plane",
            "car": "Car",
            "boat": "Boat",
            "shopping-cart": "Else",
        },

    });

    // Routes Collection
    // ---------------

    // The collection of routes is backed by *localStorage*
    // instead of a remote server.
    var RouteList = Backbone.Collection.extend({

        // Reference to this collection's model.
        model: Route,

        editing: false,

        // Save all of the items under the `"routes-app"` namespace.
        localStorage: new Backbone.LocalStorage("routes-app"),

        nextOrder: function() {
            if (!this.length) return 1;
            return this.last().get('order') + 1;
        },

        // Routes are sorted by their original insertion order.
        comparator: function(todo) {
            return todo.get('order');
        },

    });

    // Create global collection of Routes.
    var Routes = new RouteList;

    var RouteView = Backbone.View.extend({

        tagName:  "div",
        className: "row",

        template: _.template($('#item-template').html()),
        editSelectTemplate: _.template($('#edit-select-template').html()),
        editInputTemplate: _.template($('#edit-input-template').html()),
        editDatetimeTemplate: _.template($('#edit-datetime-template').html()),

        events: {
            "click .td-by>.pencil": "editBy",
            "click .td-from>.pencil": "editFrom",
            "click .td-departure>.pencil": "editDeparture",
            "click .td-to>.pencil": "editTo",
            "click .td-arrival>.pencil": "editArrival",
            "click div .ok": "closeEditing",
            "keypress div": "updateOnEnter",
            "click .destroy": "clear",
        },

        initialize: function() {
            this.listenTo(this.model, 'change', this.render);
            this.listenTo(this.model, 'destroy', this.remove);
            this.editingEl = null;
        },

        render: function() {
            // console.log('render');
            this.$el.empty();
            this.$el.append(this.template(this.model.toJSON()));
            return this;
        },

        closeEditing: function() {
            if (Routes.editing) {
                // console.log('closeEditing');
                Routes.editing.save();
                Routes.editing = false;
            }
        },

        prepareEdit: function(ev) {
            this.closeEditing();
            Routes.editing = this;
            return {
                "id": ev.target.parentNode.className.replace(/(.*)td-/, ''),
                "value": ev.target.parentNode.textContent,
            }
        },

        // Switch view into editing mode.
        editBy: function(ev) {
            this.editingEl = this.$(".td-by");
            this.prepareEdit(ev);
            
            this.$(".td-by").html(this.editSelectTemplate(
                this.model.toJSON()
            ));
        },

        editFrom: function(ev) {
            var context = this.prepareEdit(ev);
            this.$(".td-from").html(this.editInputTemplate(context));
        },

        editDeparture: function(ev) {
            var context = this.prepareEdit(ev);
            this.$(".td-departure").html(this.editDatetimeTemplate(context));
            $('.datetime').datetimepicker();
        },

        editTo: function(ev) {
            var context = this.prepareEdit(ev);
            this.$(".td-to").html(this.editInputTemplate(context));
        },

        editArrival: function(ev) {
            var context = this.prepareEdit(ev);
            this.$(".td-arrival").html(this.editDatetimeTemplate(context));
            $('.datetime').datetimepicker();
        },

        // Switch view into regular mode.
        save: function() {
            var ids = ['by', 'from', 'departure', 'to', 'arrival'];
            for (index in ids) {
                var key = ids[index];
                if ($('#'+key).val()) {
                    this.model.save(key, $('#'+key).val());
                }
            }
            this.render();
        },

        updateOnEnter: function(e) {
            if (e.keyCode == 13) {
                this.closeEditing();
            }
        },

        clear: function() {
            this.model.destroy();
        },

    });


    // The Application
    // ---------------

    var AppView = Backbone.View.extend({

        template: _.template($('#table-view').html()),

        events: {
            "click #remove-all": "removeAll",
            "submit #new-item": "createOnEnter",
        },

        initialize: function() {
            this.listenTo(Routes, 'add', this.addOne);
            this.listenTo(Routes, 'reset', this.addAll);
            // this.listenTo(Routes, 'all', this.render);

            this.footer = this.$('footer');
            this.main = $('#main');

            Routes.fetch();
            this.render();
        },

        render: function() {
            this.$el.html(this.template());
            $('#app').empty().append(this.$el);
            this.addAll();

            $('.datetime').datetimepicker();
            if (Routes.length) {
                this.main.show();
                this.footer.show();
            } else {
                this.main.hide();
                this.footer.hide();
            }
            return this;
        },

        addOne: function(route) {
            var view = new RouteView({model: route});
            this.$("#route-list").append(view.render().el);
        },

        addAll: function() {
            Routes.each(this.addOne, this);
        },

        removeAll: function() {
            // Destroy all models.
            var model;
            while (model = Routes.first()) {
                model.destroy();
            }
        },

        createOnEnter: function(ev) {
            ev.preventDefault();
            var routeData = $(ev.target).serializeObject();
            Routes.create(routeData, {validate: true});
            // Reset form for adding data.
            $('#new-item')[0].reset()
            return this;
        },

    });

    var TimelineView = Backbone.View.extend({

        template: _.template($('#timeline-view').html()),

        initialize: function() {
            Routes.fetch();
            this.render();
        },

        render: function() {
            this.$el.html(this.template());
            $('#app').empty().append(this.$el);
            var data = [];
            Routes.each(function(model) {
                data.push(
                    {
                        times: [{
                        'starting_time': Date.parse(model.attributes.departure),
                        'ending_time': Date.parse(model.attributes.arrival),
                        'label': model.attributes.from + "-" + model.attributes.to,
                    }]}
                );
            })

            var width = 1000;
            var chart = d3.timeline()
                .tickFormat({
                    format: d3.time.format("%H:%M"),
                    tickTime: d3.time.hours,
                    tickInterval: 2,
                    tickSize: 12
                })
                .width(width*4)
                .stack()
                .margin({left:70, right:30, top:0, bottom:0})
                // .hover(function (d, i, datum) {
                //     // d is the current rendering object
                //     // i is the index during d3 rendering
                //     // datum is the id object
                //     var div = $('#hoverRes');
                //     var colors = chart.colors();
                //     div.find('.coloredDiv').css('background-color', colors(i))
                //     div.find('#name').text(datum.label);
                // })
                // .click(function (d, i, datum) {
                //     alert(datum.label);
                // })
                .scroll(function (x, scale) {
                    $("#scrolled_date").text(scale.invert(x) + " to " + scale.invert(x+width));
                });

            var svg = d3.select("#timeline").append("svg").attr("width", 1000).datum(data).call(chart);
            return this;

        },

    });

    /*
        * Router
        * A management object for endpoint navigation.
    */
    var Router = Backbone.Router.extend({
        routes: {
            '': 'main',
            'timeline': 'timeline',
        },

        main: function() {
            new AppView();
        },

        timeline: function() {
            new TimelineView();
        },

    });

    // Start the application and history.
    router = new Router();
    Backbone.history.start();

    // Build a JS object from submit form.
    $.fn.serializeObject = function() {
        var unindexed_array = this.serializeArray();
        var indexed_array = {};

        $.map(unindexed_array, function(n, i){
            indexed_array[n['name']] = n['value'];
        });

        return indexed_array;
    }

});
